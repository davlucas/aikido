#!/usr/bin/env python
from tkinter import Tk, Listbox, StringVar, Button, ACTIVE, Label, Frame
import webbrowser

positions=["TACHI WAZA",
"SUWARI WAZA",
"HANDACHI WAZA"]
attaque=['AIHANMI KATATE DORI ',
'KATATE DORI ',
'KATATE RYOTE DORI ',
'RYOTE DORI ',
'KATA DORI ',
'KATA DORI MENUCHI ',
'MAWASHI GERI',
'RYOKATA DORI ',
'MUNA DORI ',
'SHOMEN UCHI ',
'YOKOMEN UCHI ',
'CHUDAN TSUKI ',
'JODAN TSUKI ',
'MAEGERI ',
'USHIRO RYOTE DORI ',
'USHIRO RYOHIJI DORI ',
'USHIRO RYOKATA DORI ',
'USHIRO ERI DORI ',
'USHIRO KATATE DORI KUBISHIME ']
riposte=['SHIHO-NAGE',
'KOKYU-NAGE',
'KOTE-GAESHI',
'IRIMI-NAGE',
'TENSHI-NAGE',
'UDE KIME-NAGE',
'UCHI KAITEN NAGE',
'SOTO KAITEN NAGE',
'HIJI KIMEOSAE',
'IKKYO',
'NIKYO',
'SANKYO',
'YONKYO',
'GOKYO']
sens=['OMOTE',
'URA']
sens.sort()
attaque.sort()
riposte.sort()
positions.sort()
def callback(event):
    webbrowser.open_new(r"https://www.youtube.com/results?search_query={}".format(" ".join(mvmt)))
def get_sens():
    mvmt.append(choix_sens.get(ACTIVE))
    label.config(fg="#9999FF",font=("Serif",14,"bold"),text="Lien cliquable\n{} ".format(" ".join(mvmt)))
    label.grid(column=0,row=2,columnspan=3)
    label.bind("<Button-1>", callback)
def get_riposte():
    mvmt.append(choix_riposte.get(ACTIVE))
    print(mvmt)
    choix_sens.grid(column=3,row=0)
    choix_sens.insert("end",*sens)
    Button(fenetre,text="4:\nchoose sens", command=get_sens).grid(column=3,row=1)
def get_attaque():
    mvmt.append(choix_attaque.get(ACTIVE))
    choix_riposte.grid(column=2,row=0)
    choix_riposte.insert("end",*riposte)
    Button(fenetre,text="3:\n choose answer", command=get_riposte).grid(column=2,row=1)
def get_position():
    global mvmt
    mvmt=[]
    mvmt.append(choix_position.get(ACTIVE))
    choix_attaque.grid(column=1,row=0)
    choix_attaque.insert("end",*attaque)
    Button(fenetre,text="2:\nchoose attack", command=get_attaque).grid(column=1,row=1)
root = Tk()
fenetre=Frame(root,width=100)
mvmt=[]
choix_position=Listbox(fenetre,height=20,width=15,bg='white',fg='black',font=("Serif",12))
Button(fenetre,text="1:\n Choose position", command=get_position).grid(column=0,row=1)
choix_attaque = Listbox(fenetre,height=20,width=30,bg='white',fg='black',font=("Serif",12))
choix_riposte = Listbox(fenetre,bg='white',fg='black',height=20,font=("Serif",12))
choix_sens = Listbox(fenetre,height=20,width=8,bg='white',fg='black',font=("Serif",12))
label=Label(fenetre)
choix_position.grid(column=0,row=0)
choix_position.insert("end",*positions)

fenetre.pack()
root.mainloop()

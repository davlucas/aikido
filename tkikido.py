#!/usr/bin/env python
from tkinter import *

Position=['Ai Hanmi handachi waza-半身半立技', 'Suwariwaza/座技', 'tachiwaza/立ち技']

Attack=['Ai hanmi katate dori-半身片手取り',
'Chūdan tsuki-中段突き',
'Eri dori - 襟取り',
'Jodan tsuki-上段突き',
'Kata dori - 肩取り',
'Kata dori men uchi - 肩取り面打ち',
'Katate dori - 片手取り',
'Katate kubi shime - 片手首絞め',
'Katate ryote dori - 片手両手取り',
'Mae geri-前蹴り',
'Mawashi geri-回し蹴り',
'Muna dori - 胸取り',
'Ryo kata dori - 両肩取り',
'Ryote dori - 両手取り',
'Shomen uchi-正面打ち',
'Sode dori - 袖取り',
'Sokumen uchi-側面打ち',
'Ushiro hiji dori - 後ろ肘取り',
'Ushiro katate dori - 後ろ片手取り',
'Ushiro ryo kata dori - 両肩取り',
'Yokomen uchi-横面打ち']

root=Tk()
first_level_frame=LabelFrame(root,text='Position')
ListPosition=Listbox(first_level_frame,width='30',height='25')
ListPosition.pack()
ListPosition.insert("end", *Position)
second_level_frame=LabelFrame(root,text='Attack')
ListAttack=Listbox(second_level_frame,width='30',height='25')
ListAttack.pack()
ListAttack.insert("end", *Attack)

first_level_frame.grid(column=0,row=0)
second_level_frame.grid(column=1,row=0)
root.mainloop()
